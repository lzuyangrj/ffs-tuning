!==============================================================
! misalign, BBA and orbit correction functions seperated from knobs.n
! by R. Yang (1/9/2019)
!==============================================================

! sigx, sigy, sigroll -> rms. alignment error hor., ver., roll
! sigk -> rms. strength error
! taking 100 um, 100 um and 200 urad and 0.1%
! comment QF1/QD0 to ease the tuning
! sigk is equilivent to sig_angle
SetBenderr[sigrol_, sigt_]:=Module[{bmg, rol0, ang0, drol, dang},
  bmg = Element["NAME", "B*{AB}"];
  {rol0, ang0} = Element[{"ROTATE", "ANGLE"}, "B*{AB}"];
  drol = sigrol*GaussRandom[Length@bmg];
  dang = sigt*GaussRandom[Length@bmg];
  Element[{"ROTATE", "ANGLE"}, "B*{AB}"] = {rol0, ang0} + {drol, ang0*dang};
  FFS["CALC NOEXP;"];
  ];

! simple and easy way to introduce quad. errs
SetQuaderr0[sigx_, sigy_, sigroll_, sigk_]:=
  Module[{i, j, nameQ, nameQi, init, dx0, dy0, droll0, kk0, Ki, errs},
    nameQ = Flatten[Element["NAME", {"Q{DF}*X", "Q{MDF}*FF"}]];
    nameQ = Drop[nameQ, -2];
    errs = {};
    Do[
      nameQi = Element["NAME", nameQ[i]//"*"];
      {dx0, dy0, droll0, kk0} = {sigx, sigy, sigroll, sigk}*GaussRandom[4];
      If[Length@nameQi<1,
        init = Element[{"DX", "DY", "ROTATE", "K1"}, nameQi];
        SetElement[nameQi,,{"DX"->init[1]+dx0, "DY"->init[2]+dy0, "ROTATE"->init[3]+droll0, "K1"->init[4]*(1+kk0)}];
        ,
        Do[
          Ki = If[nameQi[j]==nameQ[i], "K1", nameQi[j][-2,-1]]; ! K2 for Sext.
          init = Element[{"DX", "DY", "ROTATE", Ki}, nameQi[j]];
          SetElement[nameQi[j],,{"DX"->init[1]+dx0, "DY"->init[2]+dy0, "ROTATE"->init[3]+droll0, Ki->init[4]*(1+kk0)}];
          ,{j, 1, Length[nameQi]}]];
      AppendTo[errs, {dx0, dy0, droll0, kk0}]
      ,{i, 1, Length[nameQ]}];      
    FFS["FIX *; CALC NOEXP;"];
    errs];

! advanced method w/ 1-to-1 steer at each step (to avoid failture of orbit searching)
! 5 steps w/ correciton to approach the target errors
! RandomErr option added on Mar. 5, 2020 to set QD0 rolling error 
With[{def = {Correction->False, Weight->"Whole", FFSmd->False, SVD->True, RandomErr->True}},
  SetOneQuaderr[Qname_, sigx_, sigy_, sigroll_, sigk_, opt___] := Module[{op, OC, j, nameQi, init, dx0, dy0, droll0, kk0, Ki, err={}, Nt=5},
    op = Override[opt, def];
    OC = OrbitCorrection[];    
    nameQi = Element["NAME", Qname//"*"];
    {dx0, dy0, droll0, kk0} = {sigx, sigy, sigroll, sigk};
    If[RandomErr/.op, {dx0, dy0, droll0, kk0} *= GaussRandom[4]];
      
    Do[
      If[Length@nameQi<1,
        init = Element[{"DX", "DY", "ROTATE", "K1"}, nameQi];
        SetElement[nameQi,,{"DX"->init[1]+dx0/Nt, "DY"->init[2]+dy0/Nt, "ROTATE"->init[3]+droll0/Nt, "K1"->init[4]*(1+kk0)^(1/Nt)}];
        ,Do[
           Ki = If[nameQi[j]==Qname, "K1", nameQi[j][-2,-1]]; ! K2 for Sext.
           init = Element[{"DX", "DY", "ROTATE", Ki}, nameQi[j]];
           SetElement[nameQi[j],,{"DX"->init[1]+dx0/Nt, "DY"->init[2]+dy0/Nt, "ROTATE"->init[3]+droll0/Nt, Ki->init[4]*(1+kk0)^(1/Nt)}];
           ,{j, 1, Length[nameQi]}]
        ];
      FFS["CALC NOEXP;"];
      If[Correction/.op, Do[OC@Correction[Tolerance->500e-6, Weight->Weight/.op, SVD->SVD/.op],{5}]];      
      ,{Nt}];
    
    AppendTo[err, {dx0, dy0, droll0, kk0}];
    FFS["FIX *; CALC NOEXP;"];
    err];

  (* modified 28/08/2019, seperate quads. upstream and downstream of the matching section *)
  SetQuaderr[sigx_, sigy_, sigroll_, sigk_, opt___]:=Module[{op, nameQ, errs = {}, OC},
    op = Override[opt, def];
    nameQ = Flatten[Element["NAME", {"Q{DF}*X"}]];
    If[FFSmd/.op,
      nameQ = Element["NAME", "Q{MFD}*F"];
      nameQ = Drop[nameQ, -2]];
    Scan[(AppendTo[errs, SetOneQuaderr[#, sigx, sigy, sigroll, sigk, opt]];
      !FFS["draw IEX IP PEY & DX DY "//ToString[#]//"*;"];
      )&, nameQ];
    errs];
  ];

RemoveQuaderr[errs_]:=
  Module[{i, j, nameQ, nameQi, init, dx0, dy0, droll0, kk0, Ki},
    nameQ = Flatten[Element["NAME", {"Q{DF}*X", "Q{MDF}*FF"}]];
    nameQ = Drop[nameQ, -2];    
    Do[
      nameQi = Element["NAME", nameQ[i]//"*"];
      {dx0, dy0, droll0, kk0} = errs[i];
      If[Length@nameQi<1,
        init = Element[{"DX", "DY", "ROTATE", "K1"}, nameQi];
        SetElement[nameQi,,{"DX"->init[1]-dx0, "DY"->init[2]-dy0, "ROTATE"->init[3]-droll0, "K1"->init[4]/(1+kk0)}];
        ,
        Do[
          Ki = If[nameQi[j]==nameQ[i], "K1", nameQi[j][-2,-1]]; ! K2 for Sext.
          init = Element[{"DX", "DY", "ROTATE", Ki}, nameQi[j]];
          SetElement[nameQi[j],,{"DX"->init[1]-dx0, "DY"->init[2]-dy0, "ROTATE"->init[3]-droll0, Ki->init[4]/(1+kk0)}];
        ,{j, 1, Length[nameQi]}]]
      ,{i, 1, Length[nameQ]}];      
    FFS["FIX *; CALC NOEXP;"];
  ];

! SetBenderr[sigroll_, sigk_]:=

SetSexterr[sigx_, sigy_, sigroll_, sigk_]:=
  Module[{i, j, nameQ, nameQi, init, dx0, dy0, droll0, kk0, Ki, errs},
    nameQ = Flatten[Element["NAME", "S{DF}*FF"]];
    errs = {};    
    Do[
      nameQi = Element["NAME", nameQ[i]//"*"];
      {dx0, dy0, droll0, kk0} = {sigx, sigy, sigroll, sigk}*GaussRandom[4];
      If[Length@nameQi<1,
        init = Element[{"DX", "DY", "ROTATE", "K2"}, nameQi];
        SetElement[nameQi,,{"DX"->init[1]+dx0, "DY"->init[2]+dy0, "ROTATE"->init[3]+droll0, "K2"->init[4]*(1+kk0)}];
        ,
        Do[
          Ki = If[nameQi[j]==nameQ[i], "K2", nameQi[j][-2,-1]]; ! K2 for Sext.
          init = Element[{"DX", "DY", "ROTATE", Ki}, nameQi[j]];
          SetElement[nameQi[j],,{"DX"->init[1]+dx0, "DY"->init[2]+dy0, "ROTATE"->init[3]+droll0, Ki->init[4]*(1+kk0)}];
          ,{j, 1, Length[nameQi]}]];
      AppendTo[errs, {dx0, dy0, droll0, kk0}]          
      ,{i, 1, Length[nameQ]}];      
    FFS["FIX *; CALC NOEXP;"];
    errs];

RemoveSexterr[errs_]:=
  Module[{i, j, nameQ, nameQi, init, dx0, dy0, droll0, kk0, Ki},
    nameQ = Flatten[Element["NAME", "S{DF}*FF"]];
    Do[
      nameQi = Element["NAME", nameQ[i]//"*"];
      {dx0, dy0, droll0, kk0} = errs[i];
      If[Length@nameQi<1,
        init = Element[{"DX", "DY", "ROTATE", "K2"}, nameQi];
        SetElement[nameQi,,{"DX"->init[1]-dx0, "DY"->init[2]-dy0, "ROTATE"->init[3]-droll0, "K2"->init[4]/(1+kk0)}];
        ,
        Do[
          Ki = If[nameQi[j]==nameQ[i], "K2", nameQi[j][-2,-1]]; ! K2 for Sext.
          init = Element[{"DX", "DY", "ROTATE", Ki}, nameQi[j]];
          SetElement[nameQi[j],,{"DX"->init[1]-dx0, "DY"->init[2]-dy0, "ROTATE"->init[3]-droll0, Ki->init[4]/(1+kk0)}];
          ,{j, 1, Length[nameQi]}]]
      ,{i, 1, Length[nameQ]}];      
    FFS["FIX *; CALC NOEXP;"];
  ];

! ============================================================++
! quad/sext BBA
BeamBasedAlignment=Class[{}, {}, {},
  ! orb, orbit to be approached; err, accuracy of BBA
  ! mag, "S{DF}*FF"
  Move[mag_, orb_, err_]:=Module[{orb2, nes, nes2, i, j},
    If[Length@orb<>2, Print["\t Error w/ target orbit.\n"]; FFS["end;"]];
    orb2 = orb + err*GaussRandom[2, Length[orb[1]]];
    nes = Element["NAME", mag];
    Do[
      nes2 = Flatten@{Element["NAME", nes[j]//"*"]};
      Do[
        Element[{"DX", "DY"}, nes2[i]] = orb2[;,j]
        ,{i, 1, Length@nes2}];
      ,{j, 1, Length@nes}];
    FFS["CALC NOEXP"]
    ];
    
  Sextupole[orb_, err_] := Move["S{DF}*FF", orb, err];
  Sextupole[err_] := Module[{k2l, orb},
    k2l = SextOff[];
    orb=Twiss[{"DX", "DY"}, "S{DF}*FF"];
    SextOn[k2l];
    Sextupole[orb, err]
    ];
  Sextupole[] := Sextupole[0.];
  
  SkewSextupole[orb_, err_] := Move["SK%FF", orb, err];
  ! to set to the original position w/o BBA (refer to Okugi-san measurement on 29/01/2016)
  SkewSextOrigin[err_] := Module[{orb = {{527, -94, -12, -137}, {69, -762, -138, 282}}*1e-6},
    Move["SK%FF", orb, err];
    ];
    
  SkewSextupole[err_] := Module[{orb = Twiss[{"DX", "DY"}, "SK%FF"]}, SkewSextupole[orb, err]];
  SkewSextupole[] := SkewSextupole[0.];

  Octupole[orb_, err_]:=Move["OCT%FF", orb, err];
  Octupole[err_]:=Module[{orb},
    orb=Twiss[{"DX", "DY"}, "OCT%FF"];
    Print@orb;
    Octupole[orb, err]
    ];
  Octupole[]:=Octupole[0.];
  
  ! default zero misalignment, set to 100 um
  BPM[sig_:0, opt___]:=Module[{op, nm},
    nm = Element["NAME", "M{QS}*{XF}"];
    nm2={}; Scan[AppendTo[nm2, StringDrop[#,1]]&, nm];
    dxdy = Element[{"DX", "DY"}, nm2];
    dxdy+=GaussRandom[2,Length[dxdy[1]]]*sig;
    Element[{"DX", "DY"}, nm] = dxdy;
    FFS["CALC NOEXP;"];
    ];
  ]; ! end Class

! ============================================================++
! 
OrbitCorrection=Class[{}, {}, {},
  ! orbit distortion at all BPMs
  ! assume only cbpm -> has to be updated. (2019.02.20)
  ! Misalignment: BPM misalignment wrt. magnetic fiedl centre (default, ON)
  ! ScaleError: 1% scale error (rms) to BPMs
  With[{def={Misalignment->True, ScaleError->False}},
    OrbitMoni[bpm_, opt___]:=Module[{op, orbx, orby, dx0, dy0, ampx, ampy, resStrp = 5e-6, resCBPM = 1e-6},
      op = Override[opt, def];
      {orbx, orby} = Twiss[{"DX", "DY"}, bpm];
      {dx0, dy0} = Element[{"DX", "DY"}, bpm];
      orbx = orbx + resCBPM * GaussRandom[Length[orbx]];
      orby = orby + resCBPM * GaussRandom[Length[orby]];
      If[Misalignment/.op, {orbx, orby}-={dx0, dy0}];
      If[ScaleError/.op,
        {ampx, ampy} = 1+0.01*GaussRandom[2, Length@orbx];
        {orbx, orby} *={ampx, ampy}];
      {orbx, orby}]
    ];

  ! acquire hor. R-matrix from corrector to bpm
  Rmatx[corr_:X, bpm_]:= Module[{matx, i, j, loc, bx, nu},
    matx=Table[0, {j, 1, Length[bpm]}, {i, 1, Length[corr]}];
    Do[
      Do[
        loc = LINE["S", {corr[i], bpm[j]}];
        If[loc[1]<loc[2],
          {bx, nu} = Twiss[{"BX", "NX"}, {corr[i], bpm[j]}];
          matx[j, i] = Sqrt[bx[1]*bx[2]]*Sin[nu[2]-nu[1]];
          ]
        ,{i, 1, Length[corr]}]
    ,{j, 1, Length[bpm]}];
    matx
  ];

  ! acquire ver. R-matrix from corrector to bpm
  Rmaty[corr_:Y, bpm_]:=
    Module[{matx, i, j, loc, by, nu},
    matx = Table[0, {j, 1, Length[bpm]}, {i, 1, Length[corr]}];
    Do[
      Do[
        loc = LINE["S", {corr[i], bpm[j]}];
        If[loc[1]<loc[2],
          {by, nu} = Twiss[{"BY", "NY"}, {corr[i], bpm[j]}];
          matx[j,i] = Sqrt[by[1]*by[2]]*Sin[nu[2]-nu[1]];
          ]
        ,{i, 1, Length[corr]}]
    ,{j, 1, Length[bpm]}];
    matx
  ];

  ! transpose vector
  TransposeVec[array_]:=Module[{out = {}},
    Scan[AppendTo[out, {#}]&, array];
    out
    ];
  
  ! apply orbit correction
  ! limit the max. orbit distortion at Quad and Moni less than Tolerance
  ! include the power supply error of 0.1%
  ! orb0, the defined orbit to be approached, in the form of {
  Steer1to1[corr_, bpm_, orb0_, tol_, dir_, opt___] := Module[{op, i, j, matx, maty, orbx, orby, dx0, dy0, dxmax, kb, tols=10e-8, kk0, sigk=1e-8, m, x, u, wi, v, md, def={Method->"Simple"}},
    op = Join[{opt}, def];
    If[Length@bpm<>Length[orb0[1]], Print["\t Unequal length of bpms and target orbit0.\n"]; FFS["end;"]];
    If[dir=="X", matx = Rmatx[corr, bpm], If[dir=="Y", maty = Rmaty[corr, bpm], Print["\terror with Plane (X or Y)."]; FFS["end"]]]; ! response matrix
    {dx0, dy0} = orb0;
    Do[      
      {orbx, orby} = OrbitMoni[bpm];
      dxmax = If[dir=="X", Max@(Abs[orbx-dx0]), Max@(Abs[orby-dy0])];
      ! dxsum = If[dir=="X", Plus@@((orbx-dx0)^2), Plus@@((orby-dy0)^2)];
      ! Print@dxmax; Print@dxsum;
      If[dxmax>tol,
        If[dir=="X", m = matx; x = orbx-dx0, m = maty; x = orby-dy0];
        If[SVD/.op,
          (* SVD method *)
          {u, wi, v} = SingularValues[m];
          md = Transpose[v].DiagonalMatrix[wi].u; ! inverse decomposed matrix
          kb = md.x,
          kb = If["Simple"==Method/.op, Inverse[Transpose[m].m].Transpose[m].x, LinearSolve[m, x, Tolerance->tols]]];
        kk0 = Element["K0", corr];
        If[kk0=={}, Print@corr; Element["K0", corr]=kb, Element["K0", corr] = kk0+kb];
        FFS["FIX *;"];
        FFS["CALC NOEXP"],
        Break[];
        ];
      ,{j, 1, 10}];
    !Print["\tInteration: 10 times.\n"];      
    !Print["\tResidual errors: ", dxmax*1e6, " [um].\n"];
    dxmax];
    
  Steer1to1[corr_, bpm_, opt___] := Module[{d0, {tol, dir, SVDmd}={Tolerance, Plane, SVD}/.opt},
    d0 = Table[0, {Length@bpm}];
    Steer1to1[corr, bpm, {d0, d0}, tol, dir, opt]];

  ! apply orbit correction (1-to-1 correction) globally
  ! opt: Tolerance->300e-6
  With[{def={Tolerance->300e-6, Weight->"Whole", SVD->False}},
    Correction[opt___]:=
      Module[{op, keyw="MQ{MFD}*", bpm, corx, cory, max, i, tol},
        op = Override[opt, def];
        If["Whole"==Weight/.op, keyw = "M{QS}{MFD}*{XF}"];
        If["FFS"==Weight/.op, keyw = "MQ{MFD}*F"];
        
        bpm = Element["NAME", keyw];
        {corx, cory} = Element["NAME", {"ZH*", "ZV*"}];
        If["Whole"==Weight/.op,
          corx = corx[Range[1, Length[corx]]];
          cory = cory[Range[2, Length[cory]]],
          corx = corx[Range[Length[corx]-3, Length[corx]]];
          cory = cory[Range[Length[cory]-3, Length[cory]]]];
        
        Do[
          max= If["Whole"==Weight/.op,
                  Max[Abs[Twiss[{"DX", "DY"}, {keyw, "Q{FDM}*"}]]],
                  Max[Abs[Twiss[{"DX", "DY"}, {keyw, "Q{FDM}*F"}]]]];    
          If[max>(Tolerance/.op),
            Steer1to1[corx, bpm, {Tolerance->(Tolerance/.op), Plane->"X", SVD->(SVD/.op)}];
            Steer1to1[cory, bpm, {Tolerance->(Tolerance/.op), Plane->"Y", SVD->(SVD/.op)}]
            ,Break[]]
          ,{i, 1, 10}]
        ];
    ];

  ! To reproduce ATF2 operational orbit (minimize the difference between goal and bump orbit, tol=50 um)
  ! fn, the data file from ATF server
  ! OC@RealOrbit[fn, fw, Reduction->{1. 1.}];  
  With[{def={Reduction->{1., 1.}}},
    RealOrbit[fn_, fw_:"", opt___]:=Module[{op, xred, yred, dat, bpms, bpmread, strx, stry, s, orb2},
     op = Override[opt, def];
     {xred, yred} = Reduction/.op;
     dat = Import[fn, 9, SkipRow->22];
     bpms = dat[Range[Length@dat-2], 1];  ! FFS -> IP
     bpmread = Thread[dat[Range[Length@dat-2], {3,5}]];
     bpmread[1] *= (xred*1e-6);
     bpmread[2] *= (yred*1e-6);
     
     !{strx, stry} = Element["NAME", {"ZH*", "ZV*"}];
     strx = {"ZH6X","ZH7X", "ZH8X","ZH9X","ZHFB1FF", "ZH10X","ZH1FF"}; 
     stry = {"ZV6X", "ZV7X", "ZV8X",  "ZV1FF", "ZVFB1FF", "ZV9X","ZV10X", "ZV11X"};
     Steer1to1[strx, bpms, bpmread, 50e-6, "X", SVD->False]; 
     Steer1to1[stry, bpms, bpmread, 50e-6, "Y", SVD->False];
    
      If[StringLength@fw>0,
        s = LINE["S", "*{XFP}"];
        orb2 = Twiss[{"DX", "DY"}, "*{XFP}"];
        Export[fw, Thread[Join[{s}, orb2]]];
        ];
      ];
    ]; ! end, With

   With[{def={Method->"Fit"}}, 
     CalibrateSteer[str_, moni_, plane_, opt___]:=Module[{op, k0scan, k0, dx0, dx2={}, fac},
       op = Override[opt, def];
       k0scan = Range[-2,2]*1e-6;
       k0 = Element["K0", str];
       Scan[(Element["K0", str] = #;
         FFS["CAL NOEXP"];
         If[plane=="X"||plane=="x",
           dx0 = Twiss["DX", moni],
           If[plane=="Y"||plane=="y",
             dx0 = Twiss["DY", moni],
             Print["\tPlease specify the steering direction (x/y).\n"]]];
         AppendTo[dx2, dx0];)&, k0scan];
       Element["K0", str] = k0;
       FFS["CAL NOEXP"];
       res = PolynomialFit[Thread@{k0scan, dx2}, 1];
       fac = res[1,2];     
      fac];

    SlowFB[str_, moni_, dxgoal_, plane_:"X", opt___]:=Module[{op, fac, dx2, k0, dk0},
      op = Override[opt, def];
      k0=Element["K0", str];            
      If[plane=="X"||plane=="x", dx0 = Twiss["DX", moni]];
      If[plane=="Y"||plane=="y", dx0 = Twiss["DY", moni]];
      fac = CalibrateSteer[str, moni, plane, op];
      dx2 = dxgoal-dx0;
      dk0 = dx2/fac;
      Element["K0", str] = (k0+dk0);
      FFS["CAL NOEXP;"];
      fac];
    ];
  ]; ! end, Class


! scale K to the corresponding moemntum offset
! for tuning with magnet normalization error
 ScaleK[dE0_:0]:=(
   Element["ANGLE", "B*{AB}"] =  Element["ANGLE", "B*{AB}"]/(1+dE0);
   Element["K1", "Q*{XF}"] = Element["K1", "Q*{XF}"]/(1+dE0);
   Element["K2", "S*F"] = Element["K2", "S*F"]/(1+dE0); 
   Element["K2", "{BQ}*K2"] = Element["K2", "{BQ}*K2"]/(1+dE0);
   Element["K3", "*K3"] = Element["K3", "*K3"]/(1+dE0);
   Element["K4", "*K4"] = Element["K4", "*K4"]/(1+dE0);
   Element["K5", "*K5"] = Element["K5", "*K5"]/(1+dE0);
   Element["K6", "*K6"] = Element["K6", "*K6"]/(1+dE0);
   Element["K7", "*K7"] = Element["K7", "*K7"]/(1+dE0);
   Element["K8", "*K8"] = Element["K8", "*K8"]/(1+dE0);
   Element["K9", "*K9"] = Element["K9", "*K9"]/(1+dE0);
   FFS["CALC NOEXP;"]; );
