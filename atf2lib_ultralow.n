! lib for ATF2 ultralow betay studies, to seperate from atflib.n
! on atf server. 
! since 2018-11-17
! Renjun Yang (renjun.yang@cern.ch)

!==============================================================
! set start time
!==============================================================
T0[]:=Module[{},
    time0=Date[];
    Print[time0];
];

!==============================================================
! print how much time used
!==============================================================

T1[]:=Module[{},
    time1=Date[];
    timeused=(((time1[[3]]*24+time1[[4]])*60+time1[[5]])*60+time1[[6]])-(((time0[[3]]*24+time0[[4]])*60+time0[[5]])*60+time0[[6]]);
    Print[timeused," ","seconds is used"];
];

!==============================================================
! modify 2016-12-19
! Reset seed for random functions
! according to machine time
!==============================================================
 ResetSeed[]:=
   Module[{time0,time1,time2},
          time0 = FromDate[];
          time1 = ToString[time0];
          time2 = time1[-3,-1];
          SeedRandom[ToExpression[time2]]
          ];
          
!==============================================================
! random Gaussian
!==============================================================
MakeRandomGauss[NumberOfRandomNumber_,sizeG_]:=Module[{ddd},
  ddd = Table[0,{NumberOfRandomNumber}];
  grandom = Map[(sizeG*GaussRandom[])&,ddd]
];

!==============================================================
! random an integer between min. and max.
!==============================================================
RandomInteger[min_,max_]:= (* random an integer between min. and max.*)
  Module[{num,out},
         num = (max-min)*Random[]+min;
         out=If[num<Floor[num]+0.5,
                Floor[num],
                Ceiling[num]]
         ];

CreatePlainDeck[fname_,n_:"RING"]:=Module[{fn=fname,p,f},
  p=StringPosition[fn,".sad"];
  If[p<=>{},
    fn=fn[1,p[[-1,1]]-1]];
  fn=fn//".sad";
  f=OpenWrite[fn];
  Write[f,"MOMENTUM = ", MOMENTUM/1e9," GEV;"];
  Write[f,"PBUNCH = ", PBUNCH, ";"];
  Write[f,"MINCOUP = ", MINCOUP, ";"];
  Write[f,"FSHIFT = ", FSHIFT, ";"];
  FFS["TYPE;",f];
  Write[f,"LINE ",n," ="];
  WriteBeamLine[f,Format->"MAIN"];
  Write[f,";\nFFS USE =",n,";"];
  Close[f]];

!==============================================================
! build the library for the all the elements and Twiss parameters
! at the entrance of each element
! 2012-12-14 in deltai=1 case, also append to listEles/posEles
! 2016-12-19 modify deltai=1 case again ( fine for IHEP servers) 
! 2016-12-20 modify Wholeelespos = LINE["S","WholeEle"] --> WES = LINE["S","*"]
! to avoid the "null" term occur in WEs
! 2016-12-21 apply the local dispersion( PEX, PEPX, PEY, PEPY )
!==============================================================
EleLibrary[]:=
  (* way to utilize: {totalLength,matrixTwiss}=ELeLibrary[];*)
  Module[{wholeEles,wholeElesPos,totalElesNum,listEles,deltai,i},
         wholeEles=LINE["NAME","*"];
         wholeElesPos=LINE["S","*"];
         totalElesNum=Length[wholeEles];
         totalLength=LINE["S",wholeEles[-1]];

         i=1;
         listEles = {};
         posEles = {};
         numEles = {};
         While[i<totalElesNum,
               deltai = Count[wholeElesPos,wholeElesPos[i]];
               listEles = AppendTo[listEles,wholeEles[i]];
               posEles = AppendTo[posEles,wholeElesPos[i]];
               numEles = AppendTo[numEles,i];
               i=i+deltai;
               ];
	       If[i<totalElesNum,Print["   Errors: EleLibrary[]"]];
         matrixTwiss=Transpose@{Twiss["BX",listEles],Twiss["AX",listEles],Twiss["BY",listEles],Twiss["AY",listEles],Twiss["EX",listEles],Twiss["EPX",listEles],Twiss["EY",listEles],Twiss["EPY",listEles], Twiss["DX",listEles], Twiss["DPX",listEles], Twiss["DY",listEles], Twiss["DPY",listEles]};
         If[Length[Position[matrixTwiss,{}]]>0, Print["   Errors: nulls in Twiss Matrix."]];
         {posEles,totalLength,matrixTwiss,wholeEles,listEles,numEles}
    ];

!==============================================================
! advanced final distribution output at any position
!==============================================================
WriteDistributionPos[beamFinal_,pos_]:=
  Module[{Nparticles},
         Nparticles=Length[beamFinal[[2,7]]];
         fnwrite3=OpenWrite[StringJoin["dis_",pos,".dat"]];
         Do[
            WriteString[fnwrite3,beamFinal[[2,1,i]],"  ",beamFinal[[2,2,i]],"  ",beamFinal[[2,3,i]],"  ",beamFinal[[2,4,i]],"  ",beamFinal[[2,5,i]],"  ",beamFinal[[2,6,i]],"  ",beamFinal[[2,7,i]],"\n"]
            ,{i,1,Nparticles}
            ];
         Close[fnwrite3];
  ];

!==============================================================
! export .dat for the longitudinal positions of quadrupoles
!==============================================================
ExportNamePosQuad[]:=
  (* way to utilize: {totalLength,matrixTwiss}=ELeLibrary[];*)
  Module[{wholeEles,wholeElesPos,totalElesNum,posa,posb,pos2,namea,nameb,name2},
         wholeEles=LINE["NAME","*"];
         wholeElesPos=LINE["S",wholeEles];
         totalElesNum=Length[wholeEles];
         totalLength=LINE["S",wholeEles[-1]];

         posa=LINE["S","QF*"];
         posb=LINE["S","QM*"];
         pos2 = Join[posa,posb];

         namea = LINE["NAME","QF*"];
         nameb = LINE["NAME","QM*"];
         name2 = Join[namea,nameb];

         fr = OpenWrite["./name_pos.dat"];
         Do[
            Write[fr,pos2[[i]], " ",name2[[i]]]
            ,{i,1,Length[pos2]}];
         Close[fr]
    ];

! generate initial distribution and get Twiss parameters at 1th element; nn is the number of particles generated*)
GaussBeam[emitxIni_,emityIni_,sigmalIni_,sigmaeIni_,nn_, dE_:0]:=
  Module[{betax, betay, alphax, alphay, sigmax, sigmay,sigmaxrandom,sigmapxrandom,sigmayrandom,sigmapyrandom,sigmazrandom, sigmaerandom, xDist, xpDist, yDist, ypDist, zDist, eeDist, beamR},
        {betax,alphax,betay,alphay, pex0, pepx0, pey0, pepy0} = Twiss[{"BX", "AX", "BY", "AY", "PEX", "PEPX", "PEY", "PEPY"}, "^^^"];

       sigmax=Sqrt[betax*emitxIni];
       sigmay=Sqrt[betay*emityIni];

       sigmaxrandom = MakeRandomGauss[nn,sigmax];
       sigmapxrandom = MakeRandomGauss[nn,sigmax];
       sigmayrandom = MakeRandomGauss[nn,sigmay];
       sigmapyrandom = MakeRandomGauss[nn,sigmay];
       sigmazrandom = MakeRandomGauss[nn,sigmalIni];

       ! limit Gaussian cut at 3 sigma
       sigmaerandom = {};
       Do[
         If[Length[sigmaerandom]<=nn,
            singleDp = sigmaeIni*GaussRandom[];
            If[Abs[singleDp]<3*sigmaeIni,
               AppendTo[sigmaerandom,singleDp]
              ],
            Break[]
           ]
           ,{i,1,1e8}];
           
       sigmaerandom+=dE; ! artifical momentum offest/shift
       If[Length[sigmaerandom]>nn, sigmaerandom=Take[sigmaerandom,{1,-2}]]; ! avoid N = N+1

       zDist = sigmazrandom;
       eeDist = sigmaerandom;
       xDist = sigmaxrandom + sigmaerandom*pex0;
       xpDist = (sigmapxrandom - alphax*sigmaxrandom)/betax + sigmaerandom*pepx0;
       yDist = sigmayrandom + sigmaerandom*pey0;
       ypDist = (sigmapyrandom - alphay*sigmayrandom)/betay + sigmaerandom*pepy0;

       beamR = {1,{xDist,xpDist,yDist,ypDist,zDist,eeDist,Table[1,{nn}]}};
       beamR
];

! modified from Oide-san's script
! output -> sigma, mean, ...
FitDistribution=Class[{},{},{},
  With[{def={Plot->False, Cut->False}},
    FitGaussian[d_,opt___]:=Module[{op, data=CreateBin[d],a,b,x,f,g1,g2},
      op=Override[opt, def];    
      f={a/Sqrt[2],b,ConfidenceInterval,ChiSquare}/.Fit[data,a InvErf[x]+b,x,
      {a,(data[[-1,2]]-data[[1,2]])/4},{b,(data[[1,2]]+data[[-1,2]])/2},opt];
      If[Plot/.opt,
        g1=Plot[InvErf[x] f[[1]] Sqrt[2]+f[[2]],{x,-0.9999,0.9999},DisplayFunction->Identity];
        g2=ListPlot[data,DisplayFunction->Identity];
        Append[f,Show[g1,g2,opt]],
        f,
        f]];

    ! d, 1D array 
    FitGaussian2[d_, opt___]:= Module[{op, n=Length[d], dh, f, ft, a, u, v, c, m, rms, ps={}},
      op=Override[opt, def];
      !mn = Plus@@[Times@@[d,{1}]]/Plus@@(d[;;,2]);
      !std = Sqrt[Plus@@[d[;;,2]*(d[;;,1]-mn)^2/Plus@@(d[;;,2])]];
      dh = Histogram[d, opt];
      If[Cut/.op,
        Scan[If[dh[2, #]>(Max[dh[2]]*(Cut/.op)), AppendTo[ps, #]]&, Range[Length[dh[2]]]];
        dh= dh[;;, ps]];
      m = Plus@@d/n;
      rms = Sqrt[Plus@@((d-m)^2)/n];   
      ft = Fit[Thread[dh], a*Exp[-(x-u)^2/(2*v^2)]+c, x, {v, rms}, {u, m}, {a, Max[dh[2]]}, {c, 0}];
      f = {Abs@v, u, ConfidenceInterval, ChiSquare}/.ft; ! a, c aren't exported
      f];   
    ];
    
  CreateBin[x_]:=Module[{xs=Sort[x],n=Length[x],x0,x1,nb,xp,lxp,ym,yms,sn=0,xm},
    nb=Floor[Sqrt[n]];
    {x0,x1}=xs[[{1,-1}]];
    xp=Partition[xs,nb];
    ym=Plus@@[xp,{1}]/(lxp=Length/@xp);
    yms=Sqrt[Plus@@[(xp-ym)^2,{1}]]/lxp;
    xm=(sn+=#&)/@lxp;
    xm=(2*(xm-xm[[1]]/2)/sn-1);
    Thread[{xm,ym,yms}]
    ];
  InvErf[x_Real]:=Module[{y},
    y/.FindRoot[Erf[y]==x,{y,0,{-Infinity,Infinity}}]];

  ! create 1D histogram
  ! rang: {xmin, xmax}
  ! opt: Density/Norm -> Normlized or note
  With[{def={Density->False, Norm->False, SplineInterpolation->True}},
    Histogram[x_, nbin_, rang_, opt___]:=Module[{op, xs=Sort[x], {xmin, xmax}=MinMax[rang], i, ii=1, xb, xbm, yh=Table[0, {nbin}], yb={}, len},
     op = Override[opt, def];
     xb = Table[i, {i, xmin, xmax, (xmax-xmin)/(nbin+1)}];
     xbm = (xb[#+1]+xb[#])&/@Range[nbin];
     xbm /= 2;

     Scan[(If[xs[#]>xb[ii], AppendTo[yb, #]; If[ii<(nbin+1), ii++, Break[]]])&, Range[Length@xs]];
     len = Length@yb;
     yb = yb[Range[len-1]+1]-yb[Range[len-1]];
     If[Length[yb]<nbin, yb = Join[yb, Table[0, {nbin-Length[yb]}]]];
     yh+=yb;
     If[(Density/.op)||(Norm/.op), yh /=(Plus@@yh)];
     {xbm, yh}];

    ! Histogram within +/- 3*RMS region
    Histogram[x_, opt___]:=Module[{n=Length[x], nb, m, rms, ns=3},
     nb = Floor[Sqrt[n]];
     m = Plus@@x/n;
     rms = Sqrt[Plus@@((x-m)^2)/n];
     Histogram[x, nb, {m-ns*rms, m+ns*rms}, opt]];

    ! x, array;
    CalFWHM[x_, opt___]:=Module[{op, xb, xh, pk, ppk, f=10, x1, x2, i, l, r, pl, pr, fwhm},
      (* calculate FWHM directly w/o data smoothing;
         10 times more bins to locate the FWHM left/right if SplineInterpolation->True
       *)
      op = Override[opt, def];      
      {xb, xh} = Histogram[x, opt];  
      pk = Max@xh;
      ppk = Catch[Scan[(If[xh[#]==pk, Throw[#]])&, Range[Length[xh]]]];    
      If[SplineInterpolation/.op,
        hsp = Spline[Thread@{xb, xh}];
        x1 = Table[i, {i, xb[ppk], xb[1], (xb[1]-xb[ppk])/f/ppk}];
        x2 = Table[i, {i, xb[ppk], xb[-1], (xb[-1]-xb[ppk])/f/(Length[xb]-ppk)}];
        l = Catch[Scan[If[hsp[#]<(pk/2), Throw[#]]&, x1]];    
        r = Catch[Scan[If[hsp[#]<(pk/2), Throw[#]]&, x2]];
        fwhm = r-l,
        pl = Catch[Scan[If[xh[ppk-#]<(pk/2), Throw[ppk-#]]&, Range[ppk-1]]];
        pr = Catch[Scan[If[xh[ppk+#]<(pk/2), Throw[ppk+#]]&, Range[Length[xh]-ppk]]];
        fwhm = xb[pr]-xb[pl]];
        fwhm];
    ];      
 ]; ! Class[

!==============================================================
! 2017/06/01
! Import Real data from database/file
! Ncolumn -> the number of columns, has be defined!!!
! fname -> file name
!==============================================================
With[{def={append->False, SkipRow->False}},
  Export[name_, data_, head_:"", opt___] := Module[{op, nl,nw, fw},
    op = Override[opt, def]; 
    $FORM = '12.6';
    {nl, nw} = Dimensions[data];
    If[Length[Dimensions[data]]<>2, nl = Length[data]; nw = Length[data[1]]];
    System["rm "//name];
    fw = If[append/.op, OpenAppend[name], OpenWrite[name]];
    If[StringLength@head>0, WriteString[fw, head, "\n"]];
    Table[
      Table[WriteString[fw,data[i,j],"\t"],{j,1,nw}];
      WriteString[fw,"\n"];
      ,{i,1,nl}];
    Close[fw];
    $FORM = "S17.15";   
    ];

  ExportATF2Twiss[fw_, elems_:"{MQIOZ}*{XFP}"]:=Module[{s0, tws, res},
    s0 = LINE["S", elems];
    tws = Twiss[{"BX", "BY", "PEX", "PEY", "NX", "NY"}, elems];
    res =Join[{s0}, tws];
    Export[fw, Thread@res, "# s [m]\tBX [m]\tBY [m]\tEX [m]\tEY [m]\tNX [2PI]\tNY [2PI]"];
    ];

  Import[fn_, ncolumn_, opt___]:=Module[{op, fr, fg, lev1={}, lev2, nskip},
    op = Override[opt, def]; 
    fr = OpenRead[fn];
    nskip = SkipRow/.op;
    If[nskip,  Do[Read[fr, Word], {ncolumn*nskip}]];
    Do[
      fg = Read[fr, Word];  
      If[ToString[fg]=="EndOfFile",Break[]];
      lev2 = {ToExpression@fg}; 
      Scan[AppendTo[lev2, Read[fr, Real]]&, Range[ncolumn-1]];
      AppendTo[lev1, lev2]
      ,{100000}];
    lev1];
   ];
   
   
!==============================================================
! give lossmap along beam line
! beamN: input, nameEnd: end of tracking
!==============================================================
MapLoss[beamN_,nameEnd_:DSH]:= Module[{beamN2,posN={},numN={},pos1},
  beamN1 = beamN;                             
  Do[
    If[LINE["S",pos1-1]<LINE["S",pos1],
      beamN2 = TrackParticles[beamN1,pos1];
      dNum = Apply[Plus,#]&/@{beamN1[2,7],beamN2[2,7]};
      If[#>#2&@@dNum,
        AppendTo[posN,pos1];
        AppendTo[numN,#-#2&@@dNum];
        ];
      beamN1 = beamN2;
      ]
    ,{pos1,2,LINE["POSITION",nameEnd]}];
    Thread[{posN,numN}]
    ];

!==============================================================
! Accumulate lossmap (can also be applied to lossmap of signle loop
! beam -> input, nameEnd -> end element, losspre -> loss lib
! e.g. losspre = {} for 1st loop
!==============================================================
MapLossAccu[beamz_,nameEnd_,losspre_]:=Module[{posni,lossnp,losslib},
    losslib = losspre;
    lossnp = MapLoss[beamz,"DSH"];
    If[Length[lossnp]>0,
      lossnp={LINE["S",#1],#2}&@@#&/@lossnp;
      Do[
        If[MemberQ[losslib[;;,1],lossnp[i,1]],
          posni = Position[losslib[[;;,1]],lossnp[i,1]][1,1];
          losslib[posni,2]+=lossnp[i,2];
          ,
          AppendTo[losslib,lossnp[i,;;]];
          If[Length[losslib]>2,
            losslib = Sort[losslib,#1[1]<#2[1]&]; 
            ];
          ];
        ,{i,1,Length[lossnp]}]
      ];
      losslib
      ];

!==============================================================
! Evaluate transverse acceptance, y, 
! Input: nx/ny -> max. number of sigmay; npMin/npMax -> range of num.
! of rms. energy spread; dir -> "X" or "Y"
! Output: DAdata -> transverse acceptance data
!         lsdata -> loss map 
!==============================================================
DAxy[nx_,ny_,npMin_,npMax_,emitx_,emity_,sigmap_,dir_]:=
  Module[{alphax,betax,alphay,betay,zero,xDist,xpDist,yDist,ypDist,zDist,eeDist,pbeam,beam,beam2,flag,pl,DAdata={},lsdata={}},
    {alphax,betax,alphay,betay}=Twiss[{"AX","BX","AY","BY"},"^^^"];
    sigmax=Sqrt[betax*emitx];
    sigmay=Sqrt[betay*emity];
    Do[
      If[dir=="X",
        zero = Table[0,{nx+1}];
        xDist = sigmax*Range[0,nx];
        xpDist = (zero - alphax*xDist)/betax;
        yDist = zero;
        ypDist = (zero - alphay*yDist)/betay;
        zDist = zero;
        eeDist = sigmap*Table[np,{nx+1}];
        pbeam = {xDist,xpDist,yDist,ypDist,zDist,eeDist,Table[1,{nx+1}]},
        If[dir=="Y",
          zero = Table[0,{ny+1}];
          xDist = zero;
          xpDist = (zero - alphax*xDist)/betax;
          yDist = sigmay*Range[0,ny];
          ypDist = (zero - alphay*yDist)/betay;
          zDist = zero;
          eeDist = sigmap*Table[np,{ny+1}];
          pbeam = {xDist,xpDist,yDist,ypDist,zDist,eeDist,Table[1,{ny+1}]},
          Print["  error with dir->x/y setting!"];
          Break[];
       ]];

      beam = {1,pbeam};
      ! tracking & save particles
      lsdata = MapLossAccu[beam,"DSH",lsdata];
      beam2 = TrackParticles[beam,"DSH"];
      flag = beam2[[2,7]];
      pl = Flatten[Position[flag,0]]-1; ! position of lost particles, start from 0
      If[Length[pl]>0,
        AppendTo[DAdata,{np,pl[1]}]
        ];
      ,{np,npMin,npMax}];
    {DAdata,lsdata}
   ];
   
!==============================================================
! nearest steering magnet upstream
! s -> position of element (integer); q1-> ZH*, q2-> ZV*
!==============================================================
 NearbyZup[s_]:=Module[{q1=s-1,q2=s-1},
   While[LINE["NAME",q1][1,2]<>"ZH",q1--];
   While[LINE["NAME",q2][1,2]<>"ZV",q2--];
   {q1,q2}
   ];
   
! ============================================================
! Find the closest Element upstream
! ============================================================
  NearbyEle[s_]:=Module[{q1=s-1},
    While[LINE["S",q1]>=LINE["S",s],q1--];
    q1
    ];
!==============================================================
! nearest magnet
! s -> position of element (integer); q1-> Q*
!==============================================================
! nearest corrector upstream
 NearbyZdown[s_]:=Module[{q1=s+1},
   While[LINE["NAME",q1][1,2]<>"Z",q1++];
   q1
   ];
 ! closest Quad
 NearbyQ[s_]:=Module[{q1=s-1,q2=s+1},
   While[LINE["NAME",q1][1]<>"Q",q1--];
   While[LINE["NAME",q2][1]<>"Q",q2++];
   d1=LINE["S",s]-LINE["S",q1+1];
   d2=LINE["S",q2]-LINE["S",q2+1];
   If[d1>=d2,q2,q1,q1]];

 ! closest Sext.
 NearbyS[s_]:=Module[{q1=s-1,q2=s+1},
   While[LINE["NAME",q1][1,2]<>"SF",q1--];
   While[LINE["NAME",q2][1,2]<>"SF",q2++];
   d1=LINE["S",s]-LINE["S",q1+1];
   d2=LINE["S",q2]-LINE["S",q2+1];
   If[d1>=d2,q2,q1,q1]];

 ! closest Bend
 NearbyB[s_]:=Module[{q1=s-1,q2=s+1},
   While[LINE["NAME",q1][1]<>"B",
         q1--;
         If[q1==0,Break[]]
        ];
   While[LINE["NAME",q2][1]<>"B",q2++];
   d1=LINE["S",s]-LINE["S",q1+1];
   d2=LINE["S",q2]-LINE["S",q2+1];
   If[d1>=d2,q2,q1,q1]];

! ============================================================
! Local orbit bump in the Kicker region (IEX -> BS1XA)
! Orbit offset 22.5 mm in the middle of QM7 (as the max.)
! Coordinate transmission at the exit of QM7R3 (Marker after QM7)
! ============================================================
KickerOrbitBump[]:=
  Module[{},
    FFS["FREE KEX1A K0"];
    ! FIT BPM1 DX 6.9845e-3;
    FFS["FIT MQM7R2 DX 22.4995e-3"];
    Do[FFS["go"],{II,100}];
    FFS["go"];
    FFS["SAVE"];

    FFS["CALC NOEXP;"];
    FFS["type MQM7R2"];

    BLX = ExtractBeamLine[];
    posInsert=LINE["POSITION",MQM7R3];
    SetElement["Conv",,{"DX"->0,"CHI1"-> 0}];       ! initializing
    FFS["cal"];
    ! coordinate transformation parameters
    dxConv = Twiss["DX",MQM7R3];
    dpxConv = Twiss["DPX",MQM7R3];

    SetElement["Conv",,{"DX"-> dxConv, "CHI1"-> -dpxConv}];
    BLX= Insert[BLX,"Conv",posInsert+1];
    FFS["USE BLX"];
    FFS["cal"];
    ];

! ============================================================
! Split closest drit downstream into 2 drifts of equal length
! isn't test well <<<<
! ============================================================

SplitDrift[s_]:= Module[{q1=s+1,nm1,ld1,pos1},
    While[LINE["NAME",q1][1]<>"L",q1++];
    nm1 = LINE["NAME",q1];

  ! in case of the i-th order of element
    ifOrder = StringPosition[nm1,"."];  ! if order-th component
    If[Length[ifOrder]>0,
      nm1 = nm1[1,ifOrder[1,1]-1]
      ];

    {ld1, pos1} = LINE[{"L","POSITION"},q1];       ! length of the orginal drift section
    Print[nm1, " | ", ld1, " | ", pos1];
    SetElement[nm1,,{"L"->ld1/2}];
    SetElement[nm1//2,DRIFT,{"L"->ld1/2}];
    BLX = ExtractBeamLine[];
    BLX = Insert[BLX,nm1//2,pos1+1];
    FFS["USE BLX"];
    FFS["cal"];
    ];

! ============================================================
! Split downstream drift into 2 of equal length,
! + add steering magnets at the beginning of drifts 
! j is the number position of Q./Bend. and also used for naming
! the correctors
! ============================================================

SplitDriftAddCorr[s_,j_]:= Module[{q1=s+1, nm1, ld1, pos1},
    While[LINE["NAME",q1][1]<>"L",q1++];
    nm1 = LINE["NAME",q1];

  ! in case of the i-th order of element
    ifOrder = StringPosition[nm1,"."];  ! if order-th component
    If[Length[ifOrder]>0,
      nm1 = nm1[1,ifOrder[1,1]-1]
      ];

    {ld1, pos1} = LINE[{"L","POSITION"},q1];       ! length of the orginal drift section
    Print[nm1, " | ", ld1, " | ", pos1];

    SetElement[nm1,,{"L"->ld1/2}];
    SetElement[nm1//2,DRIFT,{"L"->ld1/2}];
    BLX = ExtractBeamLine[];
    BLX = Insert[BLX,nm1//2,pos1+1];
    FFS["USE BLX"];
    FFS["cal"];

    SetElement[SH//j//X2, BEND,{"K0"->1}];
    SetElement[SV//j//X2, BEND,{"K0"->0, "ROTATE"->-Pi/2}];
    SetElement[SH//j//X3, BEND,{"K0"->1}];
    SetElement[SV//j//X3, BEND,{"K0"->0, "ROTATE"->-Pi/2}];

    BLX = ExtractBeamLine[];
    BLX = Insert[BLX, SH//j//X2, pos1];
    BLX = Insert[BLX, SV//j//X2, pos1+1];
    BLX = Insert[BLX, SH//j//X3, pos1+3];
    BLX = Insert[BLX, SV//j//X3, pos1+4];
    FFS["USE BLX"];
    FFS["cal"];
    ];

! ============================================================
! Mean value of a 1D list
! ============================================================
  Mean[list_]:=  Module[{res},
    res = Plus@@list/Length[list]
    ]

! ============================================================
! standard deviation of an 1D array
! ============================================================
  StandardDeviation[list_]:= Module[{mean},
    mean = Plus@@list/Length[list];
    Sqrt[Plus@@((list-mean)^2)/Length[list]]
    ]

! IP beam size by tracking
With[{},
  IPSizeTrack[beamIn_]:=Module[{fd, beam0, beamIP, fx, fy},
    fd = FitDistribution[];
    beam0 = beamIn;
    beamIP = TrackParticles[beam0, "IP"];
    
    fx = fd@FitGaussian[beamIP[2,1], Plot->False];
    fy = fd@FitGaussian[beamIP[2,3], Plot->False];
    Thread[{fx[1], fy[1]}]
  ];
]

With[{def={ChromI->False, ChromEmit->False, ChromITw->"Null", ChromEta->False, XY->True, WaistCorrection->False, SlowFB->False, SaveParticles->False, FileHeader->"", FWHM->False, FitPDF->False, RMS->False, FitBeta->False, dE0->0, Ldir->"/Users/rj/Fellow-CERN/SAD/ATF2/lattice/"}},
  ! off-momentum IP beam size scan (sigy)
  ! with beam0 at the IEX, dfmin/dfmax<10kHz
  ! to enable ChromI or ChromEmit, one must configure ChromITw para. !!
  ! updated 20feb21,
  ! include sigx for "Linear" and "Betatron", and set XY->True as default (XY argument is useless, now)
  ! add SlowFB option to keep the orb at MQF9A and MQD10A with the purpose of approaching zero dispersion before B5FF
  ! DP0 and dE to income particles is independent, the former is for optics calculation and is the center of the momentum distribution;
  ! dE is the momentum distribution for tracking
  ! ideal FB by mean of SAD matching on Mar. 18, 2020
  ! output (Tracking->True): dp, sigy, err_sigy, sigx, err_sigy, fwhm_y, fwhm_x, rms_y, rms_x
  !
  ! updated on July 02, 2020
  ! add chromatic EMITX, Etas from SAD simu. w/ dispersion bump in the DR
  ! always chromatic IEX orbit by re-generating particles for every dE
  ! Sep. 10, 2020
  ! output of fitting beam size changes to {sigx, posx, sigy, posy, ...}
  !
  IPBandwidth[b0_, fl_, fr_, df_, opt___]:=ScanSigydE[b0, fl, fr, df, opt];
  ScanSigydE[beamIn_, dfmin_:Hz, dfmax_:Hz, dfstep_:Hz, opt___]:=
    Module[{op, fd, OC, alphac=2.14e-3, freq0=714e6, freq2p, res={}, twissIP={}, dE, beam0 = beamIn, emitx0=EMITX, emity0 = EMITY, tw0 = Twiss[{"AX", "BX", "AY", "BY"}, "^^^"], etas0=Twiss[{"PEX", "PEPX", "PEY", "PEPY"},"^^^"], {dx0, dpx0}=Twiss[{"DX", "DPX"}, "^^^"], axiex, bxiex, ayiex, byiex, orbMQF9A, orbMQD10A, emiy=emity0, emix=emitx0, beamIP, beamIP2, survive, btmp, hwx, hwy, rmsx, rmsy, ri, f, df, mode = Mode/.opt, i, k0ZH=Element["K0", ZHFB1FF], k0ZV=Element["K0", ZVFB1FF], objx, objy, errFB=100e-6, emSim, twSim, emxSp, exSp, epxSp, eySp, epySp, emx, emy},      
      op = Override[opt, def];
      fd = FitDistribution[];
      OC = OrbitCorrection[];    
      freq2p= -1/freq0/alphac;
      FFS["RADCOD; RADTAPER; DP0=0; CAL NOEXP;"];
      {axiex, bxiex, ayiex, byiex} = tw0;
      ! upgrade June 11, 2020
      orbMQF9A = Twiss[{"DX", "DY"}, "MQF9AFF"]; !OC@OrbitMoni[{"MQF9AFF"}];
      orbMQD10A = Twiss[{"DX", "DY"}, "MQD10AFF"]; !OC@OrbitMoni[{"MQD10AFF"}];
      If[~(DirectoryQ[Ldir/.op])&&((ChromEmit/.op)||(ChromEta/.op)), Print["\t Set correct directory for lib. functions."]; FFS["end;"]];

      ! based on SAD simulations on July 2, 2020
      ! very casual...
      If[ChromEmit/.op, 
        emSim = Import[(Ldir/.op)//"data_chromatic_DR_IEX/emit_DR_INTRA_1e9_20200702.dat", 8];
        emxSp = Spline[Sort[emSim[;;, {1, 2}]]]];

      If[ChromEta/.op,
        twSim = Import[(Ldir/.op)//"data_chromatic_DR_IEX/twiss_IEX_DR_INTRA_1e9_20200702.dat", 15];
        exSp = Spline[Sort[twSim[;;, {1,6}]]];
        epxSp = Spline[Sort[twSim[;;, {1,7}]]];
        eySp = Spline[Sort[twSim[;;, {1,8}]]];
        epySp = Spline[Sort[twSim[;;, {1,9}]]];
        ];
      
      Do[dE = df*freq2p;
        FFS["DP0="//ToString[dE]];
        FFS["CAL NOEXP"];              
        ! below If[...] is based on 2019 March-June experimental data fitting results
        If[(ChromITw/.op)=="2019data",
          axiex = 328.5*dE^2+9.214*dE+0.3423;
          bxiex = 2462*dE^2+127.1*dE+5.046;
          ayiex = -1564*dE^2-50.17*dE-1.467;
          byiex = 341.3*dE^2+43.61*dE+2.467;
          emiy = (1.184e4*dE^2+329.3*dE+12.04)*1e-12;
          ! emix = (2.274e2*dE^2+20.85*dE+1.2)*1e-9;  ! emix, fit from SAD simulations.
          emix = emxSp[dE]; (* 2020.07.02*)
          ]; 
        If[ChromEmit/.op, FFS["EMITX ="//emix//"; EMITY ="//emiy//";"]];

        If[(ChromI/.op)||~((ChromITw/.op)=="Null"), FFS["IEX AX "//axiex//" BX "//bxiex//" AY "//ayiex//" BY "//byiex//";"]];
        If[ChromEta/.op, FFS["IEX EX "//exSp[dE]//" EPX "//epxSp[dE]//" EY "//eySp[dE]//" EPY "//epySp[dE]//";"]];
        
        FFS["CAL NOEXP;"];
        ! re-generate beam
        beam0 = GaussBeam[EMITX, EMITY, SIGZ, DP, Length[beamIn[2,7]], dE];
        
        If[SlowFB/.op,
          objx = orbMQF9A[1] + errFB*GaussRandom[];
          objy = orbMQD10A[2] + errFB*GaussRandom[];
          FFS["ZHFB1FF K0 "//k0ZH//"; ZVFB1FF K0 "//k0ZV//"; CAL NOEXP;"]; ! reset the steers
          OC@SlowFB["ZHFB1FF", "MQF9AFF", objx, "x"];
          OC@SlowFB["ZVFB1FF", "MQD10AFF", objy, "y"];
          ! Print@{Twiss["DX", "MQF9AFF"], Twiss["DY", "MQD10AFF"]};
          ! Do[OC@Steer1to1[{"ZHFB1FF"}, {"MQF9AFF"}, orbMQF9A, 3e-5, "X", SVD->False]; ! ZHFB1FF as normal
          !    OC@Steer1to1[{"ZVFB1FF"}, {"MQD10AFF"}, orbMQD10A, 5e-5, "Y", SVD->False], {3}]
          ];
        !FFS["draw QM16FF IP DX DY & BX BY MQF9AFF||MQD10AFF;"];
        If[WaistCorrection/.op, TK@AY[-0.5, 0.5, 0.05, Track->True]; TK@AX[-0.5, 0.5, 0.05, Track->True];];
        If[mode=="Tracking",
          beamIP = TrackParticles[beam0, "IP"];
          If[SaveParticles/.op,
            Export[(FileHeader/.op)//"_df="//ToString[df]//".dat", Thread[beamIP[2]]];
            ];          
          survive = Flatten[Position[beamIP[2,7],1]];
          btmp={};
          Scan[AppendTo[btmp, beamIP[2,#,survive]]&, Range[7]];
          If[Length@survive>1,
            beamIP2 = {beamIP[1], btmp};          
            If[FitBeta/.op,
              emx = FitEmit[beamIP2[2,1], beamIP2[2,2]];
              emy = FitEmit[beamIP2[2,3], beamIP2[2,4]];
              AppendTo[res, Flatten@{dE, emx[1,{3,4}], emy[1,{3,4}]}],              
              f = If[FitPDF/.op, fd@FitGaussian2[{##}, Cut->0.1], fd@FitGaussian[{##}, Plot->False]]&@@(beamIP2[2,3]);
              If[FWHM/.op, hwy = fd@CalFWHM[beamIP2[2,3]]];
              If[RMS/.op, rmsy = StandardDeviation[beamIP2[2,3]]];
              If[XY/.op,
                f2 = If[FitPDF/.op, fd@FitGaussian2[{##}, Cut->0.1], fd@FitGaussian[{##}, Plot->False]]&@@(beamIP2[2,1]);            
                If[FWHM/.op,
                  hwx = fd@CalFWHM[beamIP2[2,1]];
                  ri = {dE, f[1], f[2], f2[1], f2[2], hwy, hwx},                
                  ri = {dE, f[1], f[2], f2[1], f2[2]}];
                If[RMS/.op, rmsx = StandardDeviation[beamIP2[2,1]]; ri=Flatten@{ri, rmsy, rmsx}],
                ri = {dE, f[1], f[3,1]};
                If[FWHM/.op, AppendTo[ri, hwy]]];              
              AppendTo[res, ri]]S];
          Clear[beamIP]];

        If[mode=="Betatron",
          FFS["DP0="//ToString[dE]//";"];
          FFS["IEX DX "//ToString[dx0+dE*Twiss["EX", "IEX"]]//";"];
          FFS["IEX DPX "//ToString[dpx0+dE*Twiss["EPX", "IEX"]]//";"];          
          FFS["CAL NOEXP;"];    
          AppendTo[res, {dE, Sqrt[Twiss["BX", "IP"]*EMITX], Sqrt[Twiss["BY", "IP"]*EMITY]}]];
       
        If[mode=="Linear",
          FFS["DP0="//ToString[dE]//";"];
          FFS["CAL NOEXP;"];    
          AppendTo[res, Flatten@{dE, Twiss[{"SIGX", "SIGY"}, "IP"]}]];
        
        If[mode=="Twiss",
          FFS["DP0="//ToString[dE]//";"];
          FFS["IEX DX "//ToString[dx0+dE*Twiss["EX", "IEX"]]//";"];
          FFS["IEX DPX "//ToString[dpx0+dE*Twiss["EPX", "IEX"]]//";"];                    
          FFS["CAL NOEXP;"];    
          AppendTo[res, Flatten@{dE, Twiss[{"BX", "AX", "BY", "AY", "PEX", "PEY", "R1", "R2", "R3", "R4"}, "IP"]}];
          ];
        If[(mode=="Betatron")||(mode=="Twiss"), FFS["IEX DX "//ToString[dx0]//" DPX "//ToString[dpx0]//";"]];
        If[ChromEmit/.op, FFS["EMITX= "//emitx0//"; EMITY ="//emity0//";"]]; ! reset IEX para.
        If[(ChromI/.op)||~((ChromITw/.op)=="Null"), FFS["IEX AX "//tw0[1]//" BX "//tw0[2]//" AY "//tw0[3]//" BY "//tw0[4]//";"]];
        If[ChromEta/.op, FFS["IEX EX "//exSp[0]//" EPX "//epxSp[0]//" EY "//eySp[0]//" EPY "//epySp[0]";"]];        
        If[SlowFB/.op, FFS["ZHFB1FF K0 "//k0ZH//"; ZVFB1FF K0 "//k0ZV//"; CAL NOEXP;"]]; ! reset the steers
        FFS["draw IEX IP BX BY & EX EY & DX DY;"];
        FFS["DP0="//(dE0/.op)//"; CAL NOEXP;"];
        ,{df, dfmin, dfmax, dfstep}];
    res];
    ];
    
!==============================================================
! COD and emittance/tune perturbations with RF freq change (ATF DR)
! export files:
! COD for all RF frequencies -> pos, bx, ax, by , ay, dx, dpx, dy, dpy
! Orbit at the EXT-IEX -> dE, BX, AX, BY, AY, DX, DPX, DY, DPY
! Tune/emit -> dE, nx, ny, nz, emitx, emity, emitz
!==============================================================
RFKnobATF=Class[{},{},{},
  Export[name_,data_]:=Module[{nl,nw, fx, i},
    $FORM = "12.6";
    {nl, nw} = Dimensions[data];
    If[Length[Dimensions[data]]<>2,
       nl = Length[data];
       nw = Length[data[1]]
      ];
    System["rm "//name];
    fx = OpenWrite[name];
    Table[
      Table[
        WriteString[fx,data[i,j]," "]
        ,{j,1,nw}];
      WriteString[fx,"\n"];
      ,{i,1,nl}];
    Close[fx];
    $FORM = 'S17.15';
    ];

  TwissWholeRing[]:=Module[{nElem, pos={}, twiss0={}, i},
    nElem = LINE["POSITION", $$$];
    Do[
      If[LINE["S", i-1]<>LINE["S", i],
        AppendTo[pos, LINE["S", i]];
        AppendTo[twiss0, Twiss[{"BX", "AX", "BY", "AY", "DX", "DPX", "DY", "DPY", "PEX", "PEY"}, i]];
        ]
       ,{i, 2, nElem}];
    {pos, twiss0}
    ];

  ClosedOrb[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, orbs, orbs2, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP"];
    e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    
    orbs = ClosedOrbit/.e;
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    orbs2 = Join[{s}, Thread@orbs];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@orbs2];

  DispersionInvariant[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, hx, hy, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP;"];
    FFS["EMIT;"];
     
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    hx = 1/Twiss["BX", #]*(Twiss["PEX", #]^2+(Twiss["BX", #]*Twiss["PEPX", #] + Twiss["AX", #]*Twiss["PEX", #])^2)&/@Range[LINE["POSITION", $$$]];
    hy = 1/Twiss["BY", #]*(Twiss["PEY", #]^2+(Twiss["BY", #]*Twiss["PEPY", #] + Twiss["AY", #]*Twiss["PEY", #])^2)&/@Range[LINE["POSITION", $$$]];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@{s, hx, hy}];

  Dispersion[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, etas, etas2, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP;"];
    FFS["EMIT;"];
    
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    etas = Twiss[{"PEX", "PEPX", "PEY", "PEPY"}, Range[LINE["POSITION", $$$]]];
    etas2 = Join[{s}, etas];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@etas2];

  EmitTwiss[dfmin_:Hz, dfmax_:Hz, dfstep_:Hz, elem_]:= Module[{e, freq0=714e6, freq2p, dE, tune, emits={}, tws = {}},
    e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    freq2p= -1/freq0/(MomentumCompaction/.e);
    Do[
      dE = df*freq2p;
      FFS["DP0="//ToString[dE]];
      FFS["FSHIFT="//ToString[df/freq0]];
      FFS["CAL NOEXP"];
      FFS["EMIT"];
      ! e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
      tune = Twiss[{"NX", "NY", "NZ"}, $$$]/2/Pi;
      AppendTo[emits, Flatten@{EMITX, EMITY, SIGZ, SIGE, FractionPart/@tune}];      
      AppendTo[tws, Twiss[{"BX", "AX", "BY", "AY", "PEX", "PEPX", "PEY", "PEPY", "DX", "DPX", "DY", "DPY", "SIGX", "SIGY"}, elem]]; ! twiss at kicker
      ,{df, dfmin, dfmax, dfstep}];
    FFS["FSHIFT=0; DP0=0"];
    emits = Prepend[Thread@emits, Table[df*freq2p, {df, dfmin, dfmax, dfstep}]];     
    tws = Prepend[Thread@tws, Table[df*freq2p, {df, dfmin, dfmax, dfstep}]];    
    Thread/@{emits, tws}
    ];
  ];

! Set IEX parameters, manully
MatchIEX=Class[{},{},{},
 GetTwiss[]:=Twiss[{"AX", "BX", "AY", "BY"}, IEX]; ! ATF DR
 ! ATF2
 SetTwiss[par_]:=Module[{},
   If[BeamLineName[]=="ATF2",
      SetElement["IEX",,{"AX"->par[1], "BX"->par[2], "AY"->par[3], "BY"->par[4]}];
      FFS["CAL NOEXP"],
      Print["\tATF2 lattice wasn't loaded!\n"];
      ]
   ];
];

! wt, scale factor
OrbitBumpCorrection = Class[{},{},{},
  LoadOrbit[fp_, nbpm_, xmax_, ymax_]:= Module[{fr, posx, posy, flag, Orbitxobj, Orbityobj},  
    fr = OpenRead[fp];
    posx = {};
    posy = {};
    
    Do[
      flag = Read[fr, Real];
      If[ToString[flag]=="EndOfFile", Break[]];
      posx = AppendTo[posx, flag*1e-6];
      posy = AppendTo[posy, Read[fr, Real]*1e-6];
      ,{i, 1, nbpm}];  
    Close[fr];

    Orbitxobj = {};
    Orbityobj = {};
    Do[
      If[Abs[posy[i]]<ymax && Abs[posx[i]]<xmax, ! && posx[i]*posy[i]<>0,
        AppendTo[Orbitxobj, {"M."//i, "X", posx[i]}];
        AppendTo[Orbityobj, {"M."//i, "Y", posy[i]}];
        ]
      ,{i, 1, Length[posx]}];
    {Orbitxobj, Orbityobj}
    ];
    
  LoadOrbit[fp_]:=LoadOrbit[fp, 96, 2e-3, 1e-3];
  
  ! to seperate from Bump[]
  Bump2[orbx_, orby_, wt:{___,___}]:=Module[{bpm, strx, stry},
    ! Print[wt//"\n"];
    DesignOptics=CalculateOptics[1,LINE["LENGTH"],Twiss["*",1],FLAG["CELL"],2]; 
    bpm=Monitor["M*"];
    strx=Steer["ZH*"]; 
    stry=Steer["ZV*"]; 

    ! read ATF BPMs data    
    ! {orbx, orby} = GetWholeRingOrbit[fp, wt];
    
    b1 = Bump[strx, Condition -> orbx];
    b2 = Bump[stry, Condition -> orby];
    MakeBump[b1, strx, DesignOptics];
    MakeBump[b2, stry, DesignOptics];
    
    FFS["CALC NOEXP"];
    
    Clear[DesignOptics, b1, b2];
    ];

  Bump2[orbx_, orby_]:=Bump2[orbx, orby, {1, 1}];

  QuadRorateError[name_, err_]:=Module[{qm, roll},
    qm = LINE["NAME", name];
    roll = err*GaussRandom[Length[qm]];
    Scan[(LINE["ROTATE",#[[1]]]=#[[2]];)&, Thread[{qm, roll}]];
    !FFS["SAVE ALL;"];
    FFS["CALC NOEXP;"];
    FFS["Reject Total;"]; 
    FFS["FIX "//name];    
    ];

  SextError[elem_, dyrms_]:=Module[{sd, dy},
    sd=LINE["NAME", elem];
    dy=dyrms*GaussRandom[Length[sd]];
    Scan[(LINE["DY",#[[1]]]=#[[2]];)&, Thread[{sd,dy}]];
    !FFS["SAVE ALL;"];
    FFS["CALC NOEXP;"];
    FFS["Reject Total;"]; 
    FFS["FIX "//elem];    
    ];

  SextBBA[name_, weight_:{1,1}, err_:0]:=Module[{sd, orb0, dx, dy},
    sd=LINE["NAME", name];
    FFS["CAL NOEXP;"];
    orb0 = Twiss[{"DX", "DY"}, name];
    {dx, dy} = orb0 + err*GaussRandom[2, Length[sd]];
    {dx, dy} *=weigth;
    Scan[(LINE["DX",#[[1]]]=#[[2]];)&, Thread[{sd, dx}]];
    Scan[(LINE["DY",#[[1]]]=#[[2]];)&, Thread[{sd, dy}]];
    FFS["CALC NOEXP;"];
    FFS["Reject Total;"]; 
    FFS["FIX "//name];        
    ];
    
  ];

  
Protect[OrbitBumpCorrection, RFKnob];